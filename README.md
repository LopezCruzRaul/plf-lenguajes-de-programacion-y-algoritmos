# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
*[#lightgreen] **Programando ordenadores en los 80 y ahora**
	*_ ¿Qué ha cambiado?
		* Antes
			*_ Lenguaje
				* El código se enfocaba en ser eficiente y\nahorrar recrusos
				* Usar otro lenguaje que no era ensamblador\nse traducía en una ejecución muy lenta
				* Los lenguajes eran más difíciles de \ninterpretar por una parsona
				* Era más cercano a la máquina
				* Interactuaba directamente con los registros de memoria y \ncaracterísticas de los periféricos, por \nejemplo: la tasa de refresco del monitor
				* Se utilizan registros, direcciones de memoria y \nlas pilas de llamadas, etc.
			*_ Ordenadores
				* Tenían mucha menos memoria, que se\ntraduce en menos potencia
				* La velocidad se medía en Hz
				* El conocimiento de las plataformas era\ntrabajo del programador
				* Requerías conocer la máquina en la que\nprogramabas
				
		* Ahora
			*_ Lenguaje
				* El código se enfoca en ser entendible e\ninterpretable por el usuario
				* El código no se traduce directamente a\nlenguaje máquina
				* Existen lenguajes que ni siquiera se\nejecutan sobre la máquina huésped
					*_ por ejemplo
						* Java
							* Se ejecuta en una máquina\nvirtual, por esto es\n"multiplataforma"
				* Ya no se requiere de "interactuar" con los registros \nde memoria
				* Se utilizan variables, matrices, objetos, aritmética \ncompleja, expresiones booleanas, bucles, hilos, etc.
				* Se conocen como lenguajes de alto nivel
					*_ como
						* Java
						* C#
						* Phyton
						* Javascript
						* PHP
			*_ Ordenadores
				* Tienen mucha más memoria y potencia
					*_ pero
						* Los programas son mucho más\npesados, lo que hace de\ncontrapeso al avance de hardware
				* La velocidad se mide en en GHz	
				* El conocimiento de las plataformas es\ntrabajo del lenguaje/compilador
		
	* Leyes
		* Moore
			*  Cada 18 meses el hardware es el doble de rápido 
				* Estamos alcanzando límites físicos que\nimpiden este desarrollo, lo que significa que \nel crecimiento en las capacidades \ndel hardware va siendo menor poco a poco (rendimiento decreciente)
		* Page
			* Cada 18 meses el software se vuelve el\ndoble de lento 
				* Se debe en parte a la programación capa\n sobre capa, donde el código se encuentra \ncada vez más alejado de la máquina y requiere \npasar por más "filtros" para su ejecución 
@endmindmap
```

# Hª de los algoritmos y de los lenguajes de programación
El diagrama se presenta como mapa mental, utilizando Markdown (leftside) en lugar de notación aritmética ("+" para nodos a la derecha y "-" para nodos a la izquierda)
```plantuml
@startmindmap

*[#lightgreen] **Algoritmos y Lenguajes**\n **de programación**
	* Lenguajes de\nprogramación
		*_ es
			* Medio de comunicación humano-máquina
		*_ debe
			* Adaptarse a las necesidades 	 
			* Ofrecer una mejor comprensión
				*_ o
					* Una mejor velocidad de ejecución
			* Ayudar al programador a la resolución de problemas
		*_ se clasifican como
			* Alto nivel
				*_ Son lenguajes orientados a la creación de algoritmos más complejos, \npermiten una mejor interpretación del código \ny deben ser traducidos por la computadora al lenguaje máquina.
				*_ como
					* VB .NET
					* Erlang
					* Ada
					* BASIC
					* Fortran
			* Bajo nivel
				*_ Son lenguajes mneumóticos, o directamente interpretables por la máquina, \nen estos se trabaja directamente con la arquitectura de la \ncomputadora en que se ejecuta
				*_ como
					* Assembler
					* Lenguaje máquina
				*_ actualmente
					* Se utiliza para cosas concretas, como programar periféricos y dispositivos
					* Algoritmos eficientes y rápidos
		* Paradigmas
			* Imperativo
				*_ el programador instruye a la máquina cómo cambiar su estado
				* Procedimental
					*_ Las instrucciones se agrupan en procedimientos
				* Orientado a objetos
					*_ Su estructura se orienta a objetos y datos
					*_ entre los que se\n encuentran
						* Java, 
						* C#
						* Fortran
						* PHP
						* Ruby
						* Perl
			* Declarativo
				*_ el programador simplemente declara las propiedades \ndel resultado deseado, pero no cómo calcularlo
				* Funcional
					*_ Surge del cálculo lambda
					*_ Basado en el uso de verdaderas funciones matemáticas
					*_ Simplificaba el código, añadía funciones como agrupación de instrucciones
					* Tiene la capacidad de crear procesos paralelos distribuidos
					*_ como
						* Erlang
						* Rust
						* Scala
						* Haskell
			* Lógico
				*_ Descompone programas en componentes lógicos y de control
				*_ Procesos paralelos distribuidos
				*_ usado para
					* Sistemas expertos
					* Demostración automática de teoremas
					* Problemas de optimización
					* Inteligencia artificial3
				*_ como
					* Prolog
			* Matemático
				*_ El resultado deseado se declara como la \nsolución de un problema de optimización
			* Reactivo
				*_ Se declara el resultado deseado con flujos \nde datos y la propagación del cambio

left side

	* Algoritmos 
		* Antecedentes
			*_ 3000 A.C.
				*_ Tablillas de arcilla con procesos para realizar cálculos
			*_ Siglo XVII
				* Telar de Jacquard
					*_ Utilizaba tarjetas perforadas para tejer patrones en tela
			*_ Siglo XIX
				* Máquina analítica
					*_ Utilizaba tarjetas perforadas

		*_ Reciben una entrada y ofrecen una salida
		*_ No da lugar a ambigüedades
		*_ Se originaron mucho antes\r de las computadoras, su primera\r implementación informática fue en\r las calculadoras
		*_ Consisten en
			* Un procedimiento para resolver\run problema, con un número\rde pasos finitos
		* Tipos
			* Cuantitativos
				*_ Son aquellos cuya resolución depende de un cálculo matemático
			* Cualitativos
				*_ Su resolución no involucra cálculos numéricos, \nsino secuencias lógicas. 
		*_ Se representan físicamente como
			* Pseudocódigo
			* Diagramas de flujo
		* Limitaciones
			* Problemas indefinibles
				*_ Tiempo de ejecución, certeza de llegar al resultado, etc
		* Costes
			* Razonable
				*_ El tiempo de ejecución crece en proporción\ral tamaño del problema y número de entradas
			* No razonable
				*_ El tiempo de ejecución crece exponencialmente\r y las variables crecen
				*_ No se pueden resolver problemas grandes
				*_ como
					* El problema del viajante
						*_ Solo con 100 "nodos" el cálculo requiere más tiempo \n que la edad del universo

@endmindmap
```
 \*Se realizó originalmente con notación aritmética pero se cambió a markdown en el commit 28: https://gitlab.com/LopezCruzRaul/plf-lenguajes-de-programacion-y-algoritmos/-/commit/d243be5446cb0b464ca658d8a0c1ad573afd4ea1

# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap

*[#lightgreen] **La evolución de los lenguajes**\n**y paradigmas de programación**
	* Lenguajes de\nprogramación
		*_ es
			* Medio de comunicación humano-máquina
		*_ debe
			* Adaptarse a las necesidades 	 
			* Ofrecer una mejor comprensión
				*_ o
					* Una mejor velocidad de ejecución
			* Ayudar al programador a la resolución de problemas
		*_ se clasifican como
			* Alto nivel
				*_ Son lenguajes orientados a la creación de algoritmos más complejos, \npermiten una mejor interpretación del código \ny deben ser traducidos por la computadora al lenguaje máquina.
				*_ como
					* VB .NET, Erlang, Ada, BASIC, Fortran
			* Bajo nivel
				*_ Son lenguajes mneumóticos, o directamente interpretables por la máquina, \nen estos se trabaja directamente con la arquitectura de la \ncomputadora en que se ejecuta
				*_ como
					* Assembler
					* Lenguaje máquina
				*_ actualmente
					* Se utiliza para cosas concretas, como programar periféricos y dispositivos
					* Algoritmos eficientes y rápidos
	* **Paradigmas de programación**
		*_ es
			* Una forma de aproximarse a la solución de un problema
		*_ tipos
			* Funcional
				*_ Surge del cálculo lambda
				*_ Basado en el uso de verdaderas funciones matemáticas
				*_ Simplificaba el código, añadía funciones como agrupación de instrucciones
				*_ Lenguajes como
					* ML, Erlang, Rust, Scala, Haskell
				*_ Utiliza 
					* funciones matemáticas
					* recursividad
			* Lógico
				*_ Descompone programas en componentes lógicos y de control
				*_ Procesos paralelos distribuidos
				*_ usado para
					* Sistemas expertos
					* Demostración automática de teoremas
					* Problemas de optimización
					* Inteligencia artificial3
				*_ Utiliza
					* Expresiones lógicas
					* Mecanismos de inferencia
					* Recursividad
					* Argumentos lógicos con un solo valor **true** o **false**
				*_ Lenguajes como
					* Prolog, Mercury y ALF
			* Programación concurrente
				*_ Emplea técnicas para resolver problemas de comunicación y sincronización
				*_ Utiliza
					* Recursividad
					* Paralelismo
					* Proceso
					* Hilos
				*_ Estructura
					* Ejecución de procesos
					* Finalización de procesos
					* Monitorización de procesos
					* Operaciones de Entrada y Salida
				*_ Su importancia radica en 3 aspectos
					* Aprovechamiento de los recursos del sistema
						*_ solo se crean los hilos necesarios
					* Establecimiento de prioridades
						*_ el orden y prioridad de ejecución es establecido por el programador
					* Multiprocesamiento real
			* Programación Orientada a objetos
				*_ Brinda una percepción más abstracta
				*_ No se parte del problema, sino de los objetos que componen el problema y la solución a este
				*_ Origen
					* Surgió al rededor de los años 70
					* Simula 67
				*_ Utiliza
					* Entidades
						* Atributos
							*_ Son las características y propiedades del objeto o entidad
						* Métodos
							*_ Que son los comportamientos de las entidades
					* Herencia
					* Cohesión
					* Abstracción
					* Polimorfismo
					* Acoplamiento
					* Encapsulamiento
					*_ etc
				*_ Lenguajes como
					* Simula 67 (el primero)
					* Smalltalk (1972 a 1980) el ejemplo canónico de la POO
					*_ GObject, Eiffel, Fortran, Java, JS, entre muchos otros, \npuramente orientados a objetos o híbridos. 
				* Tipos
					* Basada en clases
					* Basada en prototipos	
			* Programación estructurada
				*_ Utiliza
					* Subrutinas
				*_ Surgió en 1960
					*_ con la premisa
						* La sentencia GOTO, considerada perjudicial
				*_ tiene 3 estructuras básicas
					* Secuencia
					* Selección 
						*_ Condiciones como IF y Switch
					* Iteración
						*_ Bucles como FOR y WHILE
				*_ Lenguajes como
					* ALGOL, Pascal, PL/I, Ada 				
@endmindmap
```

Se realizaron cambios en cada uno de los mapas con el fin de enriquecer la información.